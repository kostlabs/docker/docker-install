# Docker Install

Install Docker & Docker Compose on centos / debian based systems.

# Limits

- Script works only on Linux systems

# Установка

Clone repository and execute the script

```
git clone https://gitlab.com/itwebsolutions/docker-install &&
    cd docker-install &&
    chmod +x install.sh &&
    sudo bash install.sh
```

## Authors

* **Chris** - *Getting errors after errors* - [KostLinux](https://github.com/KostLinux)
