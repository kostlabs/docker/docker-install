#!/bin/bash
if [ "$EUID" != "0" ]
  then echo "Please run as root! Exiting..."
  exit
fi
OS_RELEASE=$(cat /etc/os-release  | head -1 | cut -d '"' -f 2)
CHECK_USER=$(whoami)

if [[ "$OS_RELEASE" == "Ubuntu" ]];
then
    sudo bash scripts/ubuntu.sh

elif [[ "$OS_RELEASE" == "CentOS Linux" ]];
then
    sudo bash scripts/centos.sh
else
    echo "Your $OS_RELEASE is not supported by this script."
fi
