echo "Installing all needed dependencies..."
    apt update -y ; apt install curl -y

echo "Getting docker installing script and starting docker install..."
    curl -fsSL https://get.docker.com/ | sh

echo "Changing user rights to start docker containers as $CHECK_USER."
    usermod -a -G docker $CHECK_USER > /dev/null
        if [ "$?" -eq "0" ];
        then
            echo "User rights set."
        else
            echo "Something went wrong."
            exit

        fi
echo "Restarting docker daemon..."
    systemctl restart docker > /dev/null
        if [ "$?" -eq "0" ];
        then
            echo "Docker daemon restart success!"
        else
            echo "Something went wrong."
            exit

        fi

echo "Getting docker-compose..."
    curl -L https://github.com/docker/compose/releases/download/v2.3.3/docker-compose-$(uname -s)-$(uname -m) -o /usr/bin/docker-compose > /dev/null
        if [ "$?" -eq "0" ];
        then
            echo "Docker Compose installed successfully!"
        else
            echo "Something went wrong."
            exit

        fi
echo "Setting execution permission to compose"
    chmod +x /usr/bin/docker-compose > /dev/null
        if [ "$?" -eq "0" ];
        then
            echo "Docker Compose execution permission set."
        else
            echo "Something went wrong."
            exit
        fi

echo "Checking for docker-compose application"
    docker-compose -v > /dev/null
        if [ "$?" -eq "0" ];
        then
            echo "Docker & Docker Compose installed success! Exiting.."
            echo "Don't forget to relogin. Basically relogin to your linux machine."
        else
            echo "Something went wrong."
            exit
        fi
